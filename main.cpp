/* Project 5,  Sorting Algorithms
 * CS 5610
 * Pooya Taherkhani,  pt376511 @ ohio edu
 * Sun May 14 17:38:11 EDT 2017
 */

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <cstddef>
#include "list.h"

using namespace std;

void usage() {
  cout << "Usage: ./project5 [option] < input_file > output_file" << endl 
       << endl 
       << "  Options: " << endl 
       << "    -s  performs shellsort on input array then displays results"      << endl 
       << "    -i  performs insertion sort on input array then displays results" << endl 
       << "    -l  performs quicksort on input array, using the last element"    << endl 
       << "          as the pivot, then displays result"                         << endl 
       << "    -m  performs quicksort on input array, using the median of"       << endl 
       << "          the first, middle and last elements as the pivot, then"     << endl  
       << "          displays results"                                           << endl 
       << " [no option]  by default, performs quicksort on input array, using"   << endl
       << "              the first element as the pivot, then displays result"   << endl
       << " [more than one option]  only the first option will take effect"      << endl
       << "                         -slim is the same as -s"  << endl
       << endl;

  exit(EXIT_FAILURE);
}

int main(int argc, char* argv[]) {

  // usage();

  //** load the list
  List int_array;		// array of integers
  int int_count;		// number of integers
  unsigned long long int comp_count = 0; // number of key comparisons
  int_array.input(int_count);
  cout << int_count;
  // int_array.output();

  //** perform the desired sort algorithm
  switch (argc) {
  case 1:			// quicksort, fist element as pivot
    int_array.quicksort(0, int_count-1, 'f', comp_count);
    break;
  case 2:
    switch (argv[1][1]) {
    case 's':			// shellsort
      comp_count = int_array.shellsort(int_count);
      break;
    case 'i':			// insertion sort
      comp_count = int_array.insertion_sort(int_count);
      break;
    case 'l':			// quicksort, last element as pivot
      int_array.quicksort(0, int_count-1, 'l', comp_count);
      break;
    case 'm':	 // quicksort, median of first, last, and middle element as pivot
      int_array.quicksort(0, int_count-1, 'm', comp_count);
      break;
    default:
      cout << "invalid option!\n";
    }
    break;
  default:
    cout << "at most one argument is allowed!\n";
  }

  // int_array.output();
  cout << ',' << comp_count << endl;

  return EXIT_SUCCESS;
}
