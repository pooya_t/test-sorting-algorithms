/* Project 5,  Sorting Algorithms
 * CS 5610
 * Pooya Taherkhani,  pt376511@ohio.edu
 * Sun May 14 17:38:11 EDT 2017
 */

#ifndef LIST_H
#define LIST_H

#include <vector>

class List {
 private:
  std::vector<int> array;

  int partition_first(int start_idx, int end_idx, unsigned long long int& comp_count);
  int partition_last(int start_idx, int end_idx, unsigned long long int& comp_count);
  int partition_median(int start_idx, int end_idx, unsigned long long int& comp_count);
  void swap(int& a, int& b);
  int median_idx(int i, int j, int k, unsigned long long int& comp_count);

 public:
  void input(int& count);
  void output();
  void quicksort(int start_idx, int end_idx, char pivot_type,
                 unsigned long long int& comp_count);
  unsigned long long int shellsort(int int_count);
  unsigned long long int insertion_sort(int int_count);
};

#endif
